from rest_framework import generics
from rest_framework.viewsets import GenericViewSet

from events.models import Event
from events.serializers.events_serializers import (
    EventListSerializer,
    EventRetrieveSerializer,
)
from libs.pagination import StandardResultsSetPagination


class EventsViewSet(
    GenericViewSet,
    generics.ListAPIView,
    generics.RetrieveAPIView,
):
    pagination_class = StandardResultsSetPagination
    queryset = Event.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return EventListSerializer

        return EventRetrieveSerializer
