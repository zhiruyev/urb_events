from django.contrib import admin

from events.models import (
    Event,
    EventPreviewImage,
    EventFile,
)


class EventPreviewImageInline(admin.TabularInline):
    model = EventPreviewImage


class EventFilesInline(admin.TabularInline):
    model = EventFile


@admin.register(Event)
class EventsAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'title')
    list_display_links = ('uuid', 'title')
    search_fields = ('uuid', 'title')

    inlines = [
        EventPreviewImageInline,
        EventFilesInline,
    ]
