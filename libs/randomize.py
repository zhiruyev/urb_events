import random
from string import ascii_lowercase, digits


def get_random_string(length):
    y = "".join(random.choice(ascii_lowercase + digits) for _ in range(length))
    return y
