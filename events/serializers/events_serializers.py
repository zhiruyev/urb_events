from rest_framework import serializers

from events.models import (
    Event,
    EventFile,
    EventPreviewImage,
)


class EventFileListSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventFile
        fields = [
            'uuid',
            'type',
            'file'
        ]


class EventPreviewImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventPreviewImage
        fields = [
            'image',
        ]


class EventListSerializer(serializers.ModelSerializer):
    """Serializer for event list"""
    preview_image = EventPreviewImageSerializer()

    class Meta:
        model = Event
        fields = (
            'uuid',
            'title',
            'description',
            'start_date',
            'preview_image',
        )


class EventRetrieveSerializer(serializers.ModelSerializer):
    """Serializer for event list"""
    preview_image = EventPreviewImageSerializer()
    files = EventFileListSerializer(many=True)

    class Meta:
        model = Event
        fields = (
            'uuid',
            'title',
            'description',
            'body_text',
            'start_date',
            'preview_image',
            'files',
        )
