from libs.randomize import get_random_string


def file_events_upload(instance, filename):
    y = get_random_string(25)
    extension = filename.split(".")[-1]
    return f"events/event_files/{y}.{extension}"
