from uuid import uuid4

from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.utils.translation import gettext as _

from events.constants import FileType
from events.utils.file_upload import file_events_upload


class Event(TimeStampedModel):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    body_text = models.TextField()
    start_date = models.DateTimeField()
    location = models.CharField(max_length=100)

    class Meta:
        verbose_name = _('Событие')
        verbose_name_plural = _('События')

    def __str__(self):
        return self.title


class EventFile(models.Model):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False)
    event = models.ForeignKey(Event, blank=True, on_delete=models.CASCADE, related_name='files')
    type = models.PositiveSmallIntegerField(choices=FileType.choices(), default=FileType.TYPE_IMAGE)
    file = models.FileField(upload_to=file_events_upload)

    class Meta:
        verbose_name = _('Файл события')
        verbose_name_plural = _('Файлы событий')


class EventPreviewImage(models.Model):
    image = models.ImageField(upload_to=file_events_upload)
    event = models.OneToOneField(
        Event,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='preview_image',
    )

    class Meta:
        verbose_name = _('Файл превью события')
        verbose_name_plural = _('Файлы превью событий')
