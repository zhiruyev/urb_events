from rest_framework.routers import DefaultRouter
from events.views import events_views

app_name = 'registration'

router = DefaultRouter()
router.register(
    'events',
    events_views.EventsViewSet,
    basename='events'
)

urlpatterns = router.get_urls()
